/*==================================================================

	===============================
	 [ZombieMod] Round End Control
	===============================
	
	파일: zm_roundend.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: CS_TerminateRound와 좀비사살 중 라운드를 종료시킵니다

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 다음 라운드 대기시간 : 라운드 종료에 사용
#define NEXTROUND_WAITTIME 5.0


//============================================================
// 변수
//============================================================
// 라운드 종료를 위한 타이머.
new Handle:tRoundEnd;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name		= "[ZombieMod] Round End",
	author 		= ZM_PRODUCER,
	description = "Manages the round ending",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 게임 이벤트
	HookEvent("round_start", Event_RoundStart);
	HookEvent("round_freeze_end", Event_RoundFreezeEnd);
	HookEvent("round_end", Event_RoundEnd);
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Round End", ZM_LANG_PREFIX);
}

/**
 * 맵이 시작될 때
 */
public void OnMapStart()
{
	// 타이머 초기화.
	tRoundEnd = INVALID_HANDLE;
}


//============================================================
// 콜백
//============================================================
/**
 * 게임이벤트; 라운드 시작
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	RoundManager_DeleteTimer();
}

/**
 * 게임이벤트; 라운드 프리즈 엔드
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundFreezeEnd(Event event, const char[] name, bool dontBroadcast)
{
	RoundManager_OnRoundFreezeEnd();
}

/**
 * 게임이벤트; 라운드 종료
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	RoundManager_DeleteTimer();
}

/**
 * 타이머; 라운드 종료
 * 이는 0:00에 동작합니다.
 *
 * @param timer 		타이머 핸들
 */
public Action RoundManager_Timer_RoundEnd(Handle:timer)
{
	// 타이머 초기화
	tRoundEnd = INVALID_HANDLE;
	
	// 인간이 아니면 라운드 시간이 다됐을 때 사살.
	// x = 클라이언트 인덱스.
	for (new x = 1; x <= MaxClients; x++)
	{
		// 게임에 없습니다
		if (!IsClientInGame(x))
			continue;
		
		// 살아있지 않네요
		if (!IsPlayerAlive(x))
			continue;
		 
		// 라운드 종료
		EndRound(x, false);
	}
}


//============================================================
// 일반
//============================================================
/**
 * 타이머 제거
 */
void RoundManager_DeleteTimer()
{
	// 라운드 종료 타이머가 동작중이라면 제거.
	if (tRoundEnd != INVALID_HANDLE)
		KillTimer(tRoundEnd);
	
	// 핸들 초기화.
	tRoundEnd = INVALID_HANDLE;
}

/**
 * 라운드프리즈엔드에서 라운드종료타이머 생성
 */
void RoundManager_OnRoundFreezeEnd()
{
	// 타이머 초기화
	if (tRoundEnd != INVALID_HANDLE)
		tRoundEnd = INVALID_HANDLE;
	
	// 라운드 시간을 60.0 초 단위로 변환하여 이를 타이머 발동 시간으로 지정하고
	new Float:server_roundtime = GetConVarFloat(FindConVar("mp_roundtime"));
	server_roundtime *= 60.0;
	
	// 타이머 생성.
	tRoundEnd = CreateTimer(server_roundtime, RoundManager_Timer_RoundEnd, _, TIMER_FLAG_NO_MAPCHANGE);
}

/**
 * 라운드 종료 방법을 선택합니다
 * 
 * @param client 		클라이언트 인덱스
 * @param kill 			설정시 좀비들을 죽입니다, 아니라면 CT가 이긴것으로 라운드 종료
 */
void EndRound(int client, bool kill = false)
{
	if (kill)
	{
		if (IsClientZombie(client))
			ForcePlayerSuicide(client);

		return;
	}

	CS_TerminateRound(NEXTROUND_WAITTIME, CSRoundEnd_CTWin);
}
