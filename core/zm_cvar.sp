/*==================================================================

	======================================
	 [ZombieMod] Console variable control
	======================================
	
	파일: zm_cvar.sp
    언어: SourcePawn
    방식: 절차형
    종류: 모듈
	설명: 팀 균형잡기, 팀 제한과 같은 서버콘솔명령어를 제어

    제작: Vingbbit
    웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 제한할 서버 명령어의 값
#define CVARS_AUTOTEAMBALANCE_LOCKED 0
#define CVARS_LIMITTEAMS_LOCKED 0


//============================================================
// 변수
//============================================================
// 콘솔명령어 핸들(이 핸들에 해당 콘솔명령어 이식 후 제어합니다)
new Handle:g_hAutoTeamBalance = INVALID_HANDLE;
new Handle:g_hLimitTeams = INVALID_HANDLE;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Console Variable Manager",
	author 		= ZM_PRODUCER,
	description = "Manages the specified console variables.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 차단할 콘솔명령어 찾기/제어
	FindConvarToBlock();

    // 서버에 알림
	PrintToServer("%s Enable :: Manager :: Console Variables", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * Cvar hook callback (mp_autoteambalance, mp_limitteams)
 * 콘솔명령어 값이 변경되는 것을 차단합니다..
 * 
 * @param convar    콘솔명령어 핸들
 * @param oldvalue  변경되기 전의 값 (The value before the attempted change.)
 * @param newvalue  새로운 값
 */
public void CvarsHookLocked(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
    // 콘솔명령어가 mp_autoteambalance라면 계속한다.
    if (cvar == g_hAutoTeamBalance)
    {
        // 입력된 값이 새로운 값과 일치 한다면 정지.
    	// 이미 이 값으로 지정이 되어있으니 아래의 작업을 할 필요가 없으므로 정지시키는 것입니다.
        if (StringToInt(newvalue) == CVARS_AUTOTEAMBALANCE_LOCKED)
            return;
        
        // 지정한 고정값으로 되돌립니다.
        SetConVarInt(g_hAutoTeamBalance, CVARS_AUTOTEAMBALANCE_LOCKED);
        
        // 위에서 행동한 것을 서버에 알립니다.
        PrintToServer("%s Cvar \"mp_autoteambalance\" was reverted back to %d.", ZM_LANG_PREFIX, CVARS_AUTOTEAMBALANCE_LOCKED);
    }

    // 콘솔명령어가 mp_limitteams라면 계속한다.
    else if (cvar == g_hLimitTeams)
    {
        // 입력된 값이 새로운 값과 일치 한다면 정지.
        if (StringToInt(newvalue) == CVARS_LIMITTEAMS_LOCKED)
            return;
        
        // 지정한 고정값으로 되돌립니다.
        SetConVarInt(g_hLimitTeams, CVARS_LIMITTEAMS_LOCKED);
        
        // 위에서 행동한 것을 서버에 알립니다.
        PrintToServer("%s Cvar \"mp_limitteams\" was reverted back to %d.", ZM_LANG_PREFIX, CVARS_LIMITTEAMS_LOCKED);
    }
}


//============================================================
// 일반
//============================================================
/**
 * 차단할 서버콘솔명령어를 찾고, 내용 상단에서 지정한 값을 강제 정의합니다.
 */
void FindConvarToBlock()
{
    // 차단할 서버콘솔명령어를 해당 핸들에 심어줍니다.
    g_hAutoTeamBalance = FindConVar("mp_autoteambalance");
    g_hLimitTeams = FindConVar("mp_limitteams");

    // 값을 지정시킵니다.
    SetConVarInt(g_hAutoTeamBalance, CVARS_AUTOTEAMBALANCE_LOCKED);
    SetConVarInt(g_hLimitTeams, CVARS_LIMITTEAMS_LOCKED);

    // 서버 자체나 관리자에 의해 바뀌는 것을 막기 위해 고리걸기(hook)
    HookConVarChange(g_hAutoTeamBalance, CvarsHookLocked);
    HookConVarChange(g_hLimitTeams, CvarsHookLocked);
}
