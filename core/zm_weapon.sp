/*=======================================================================

	============================
	 [ZombieMod] Weapon Manager
	============================
	
	파일: zm_weapon.sp
	언어: SourcePawn
    방식: 절차형
    종류: 모듈
	설명: 인간이 아닌 플레이어의 대한 무기 사용을 제어합니다.

	개정: CS:GO에서 좀비가 다른 무기를 주울 수 있던 것을 방지(2018/June/8)

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
=======================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Weapon Manager",
	author 		= ZM_PRODUCER,
	description = "Manages the player weapon.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버에 알림.
	PrintToServer("%s Enable :: Manager :: Weapon", ZM_LANG_PREFIX);
}

/**
 * 클라이언트가 서버 입장 시
 * 
 * @param client		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	// 무기 사용 여부를 조정하기 위해 SDKHooks 사용
	// SDKHook_WeaponCanUse, SDKHook_WeaponEquip 한 곳에 포워딩.
	SDKHook(client, SDKHook_WeaponCanUse, WeaponRestrictForZombie);
	SDKHook(client, SDKHook_WeaponEquip, WeaponRestrictForZombie);
}

/**
 * 클라이언트가 서버 퇴장 시
 * 
 * @param client		클라이언트 인덱스
 */
public void OnClientDisconnect(int client)
{
	// SDKHooks 해제.
	SDKUnhook(client, SDKHook_WeaponCanUse, WeaponRestrictForZombie);
	SDKUnhook(client, SDKHook_WeaponEquip, WeaponRestrictForZombie);
}


//============================================================
// 콜백
//============================================================
/**
 * 무기 모듈; 무기를 사용할 때
 * 
 * @param client	클라이언트 인덱스
 * @param weapon	무기 인덱스
 */
public Action WeaponRestrictForZombie(int client, int weapon)
{
	// 플레이어가 인간이 아닌 경우, 무기 사용 또는 줍기를 막는다.
	if (!IsClientHuman(client))
		return Plugin_Handled;

	// 무기 클래스명 획득
	char weaponentity[32];
	GetEdictClassname(weapon, weaponentity, sizeof(weaponentity));
	
	// 무기가 칼이면 사용 또는 줍기를 허용
	if (StrEqual(weaponentity, "weapon_knife"))
		return Plugin_Continue;
	
	return Plugin_Continue;
}
