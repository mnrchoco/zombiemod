/*==================================================================

	========================
	 [ZombieMod] Engine API
	========================
	
	파일: zm_api_engine.sp
	언어: SourcePawn
	방식: 절차형
	종류: API 처리기
	설명: engine.inc의 내용을 처리합니다.
	      숙주좀비 스폰여부/설정
	      감염가능 설정/해제/확인

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 엔진 제어
bool bZombieSpawned;
bool bStopInfect;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Engine API",
	author		= ZM_PRODUCER,
	description = "Process Engine API",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 로드 시 네이티브와 포워드를 만듭니다.
 */
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	// 네이티브
	CreateNative("IsZombieSpawned", API_IsZombieSpawned);
	CreateNative("IsStopInfect", API_IsStopInfect);
	CreateNative("SetZombieSpawned", API_SetZombieSpawned);
	CreateNative("SetInfectStop", API_SetInfectStop);
	
	// 결과를 성공적이라고 반영!
	return APLRes_Success;
}

/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버에 알림
	PrintToServer("%s Enable :: API Processor :: Engine", ZM_LANG_PREFIX);
}


//============================================================
// 네이티브 처리
//============================================================
/**
 * API; 좀비가 나타났(스폰됐)나요?
 *
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_IsZombieSpawned(Handle plugin, int numParams)
{
	return bZombieSpawned;
}

/**
 * API; 감염이 정지됐나요?
 *
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_IsStopInfect(Handle plugin, int numParams)
{
	return bStopInfect;
}

/**
 * API; 좀비가 스폰된것으로 설정
 *
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_SetZombieSpawned(Handle plugin, int numParams)
{
	// engine.inc에서 지정해놓은 파라메터를 변수를 지정하여 불러오기.
	bool set = view_as<bool>(GetNativeCell(1));
	P_SetZombieSpawned(set);
}

/**
 * API; 감염 중지
 *
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_SetInfectStop(Handle plugin, int numParams)
{
	// engine.inc에서 지정해놓은 파라메터를 변수를 지정하여 불러오기.
	bool set = view_as<bool>(GetNativeCell(1));
	P_SetInfectStop(set);
}


//============================================================
// 일반
//============================================================
/**
 * 프로세스; 숙주좀비가 나옴/안나옴을 설정
 *
 * @param set 		설정시 좀비가 나온것으로 표시
 */
P_SetZombieSpawned(bool set = false)
{
	// true면 좀비가 스폰된 것
	if (set) 
		bZombieSpawned = true;
	
	// false면 스폰되지 않은 것.
	else bZombieSpawned = false;
}

/**
 * 프로세스; 감염방지장치를 설정/해제
 *
 * @param set 		설정시 감염이 중지됩니다
 */
P_SetInfectStop(bool set = false)
{
	// true면 감염 중지
	if (set) 
		bStopInfect = true;
	
	// false면 해제
	else bStopInfect = false;
}
