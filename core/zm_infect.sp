/*==================================================================

	===============================
	 [ZombieMod] Infection manager
	===============================
	
	파일: zm_infect.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 라운드 시작 전 대기시간(round_freeze_end)이 끝나고
		  첫번째 좀비를 타이머를 통해 만들고 플레이어가 좀비에게
		  타격을 입으면 감염 시키는 내용을 담았습니다.
		  
		  이 좀비모드는 CS_TerminateRound를 사용하지 않고 
		  인간팀(CT)에 한명만 있는 경우 감염하지 않습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:DELAY,
	Handle:PLAYERRATIO
}
new cv[CVAR];

// 숙주 감염 타이머.
new Handle:tInfect;		


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Infect Manager",
	author 		= ZM_PRODUCER,
	description = "Manages Infect/Cure function.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}
	

//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[DELAY] 		= CreateConVar( "zm_start_delay",
									"10",
									"게임이 시작되기 전까지 대기할 시간");
	
	cv[PLAYERRATIO] = CreateConVar( "zm_player_ratio",
									"5",
									"숙주 비율");
	
	// 게임 이벤트
	HookEvent("round_start", 		Event_RoundStart);
	HookEvent("round_freeze_end", 	Event_RoundFreezeEnd);
	HookEvent("round_end", 			Event_RoundEnd);
	HookEvent("player_spawn", 		Event_PlayerSpawn, 			EventHookMode_Post);
	HookEvent("player_hurt", 		Event_PlayerHurt);
	HookEvent("player_death", 		Event_PlayerDeath);
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Infection", ZM_LANG_PREFIX);
}

/**
 * 맵이 시작될 때
 */
public void OnMapStart()
{
	// 타이머 변수 초기화.
	tInfect = INVALID_HANDLE;
	
	// 좀비는 스폰되지 않았습니다.
	SetZombieSpawned(false);
	SetInfectStop(false);
}

/**
 * 클라이언트 접속 시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	// 클라이언트를 치유(生 상태의 인간)으로 설정해놓습니다.
	CureClient(client);
}


//============================================================
// 콜백
//============================================================
/**
 * 게임이벤트; 라운드 시작
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	// 감염 타이머 제거
	if (tInfect != INVALID_HANDLE)
		KillTimer(tInfect);
	
	// 타이머 변수 초기화.
	tInfect = INVALID_HANDLE;
	
	// 좀비는 스폰되지 않았고, 감염방지장치도 동작하지 않습니다.
	SetZombieSpawned(false);
	SetInfectStop(false);
}

/**
 * 게임이벤트; 라운드 프리즈 타임 종료
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundFreezeEnd(Event event, const char[] name, bool dontBroadcast)
{
	Infect_OnRoundFreezeEnd();
}

/**
 * 게임이벤트; 라운드 종료.
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	Infect_OnRoundEnd();
}

/**
 * 게임이벤트; 플레이어 스폰 (POST)
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public void Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	// 클라이언트 인덱스
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	// 플레이어가 살아있지 않으면 정지.
	if (!IsPlayerAlive(client))
		return;
	
	// 인간으로 설정.
	CureClient(client);
}

/**
 * 게임이벤트; 플레이어가 데미지를 입었을 때
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_PlayerHurt(Event event, const char[] name, bool dontBroadcast)
{
	// 피해자(클라이언트)와 공격자를 획득합니다.
	int id = GetClientOfUserId(GetEventInt(event, "userid"));
	int at = GetClientOfUserId(GetEventInt(event, "attacker"));

	// 공격자가 유효하지 않으면 정지.
	if (!IsClientValid(at))
		return Plugin_Continue;

	// 공격자가 있나요?
	if (!IsClientInGame(at))
		return Plugin_Continue;

	// 공격자가 유효하지 않으면 정지.
	if (!IsClientValid(id))
		return Plugin_Continue;

	// 감염대상자가 있나요?
	if (!IsClientInGame(id))
		return Plugin_Continue;
	
	// 사용한 무기를 획득합니다.
	char curweapon[32];
	GetEventString(event, "weapon", curweapon, sizeof(curweapon));
	
	// player_hurt에서 감지해야될 감염관련 부분
	Infect_OnClientHurt(id, at, curweapon);

	return Plugin_Continue;
}

/**
 * 게임이벤트; 플레이어가 죽었을 때
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	// 플레이어 수 계산.
	//CheckPlayers();
	CheckHumans();
}


//============================================================
// 일반
//============================================================
/**
 * 감염모듈; 클라이언트가 데미지를 입었을 때
 * 
 * @param client 	데미지를 입은 대상
 * @param attacker 	클라이언트에게 데미지를 입힌 대상
 * @param weapon 	데미지를 입힌 무기
 */
void Infect_OnClientHurt(int client, int attacker, const char[] weapon)
{
	// 감염방지장치가 작동할 경우 정지.
	if (IsStopInfect())
		return;
	
	// 공격자가 유효하지 않으면 정지.
	if (!IsClientValid(attacker))
		return;

	// 공격자가 있나요?
	if (!IsClientInGame(attacker))
		return;

	// 공격자가 살아있나요?
	if (!IsPlayerAlive(attacker))
		return;
	
	// 감염대상자가 유효하지 않으면 정지.
	if (!IsClientValid(client))
		return;

	// 감염대상자가 있나요?
	if (!IsClientInGame(client))
		return;

	// 감염대상자가 살아있나요?
	if (!IsPlayerAlive(client))
		return;

	// 피해자가 인간 또는 생존자가 아닐 경우 정지.
	if (!IsClientHuman(client))
		return;
	
	// 무기가 칼이 아닐 경우 정지.
	if (!StrEqual(weapon, "knife"))
		return;
	
	// 좀비로 감염.
	InfectClient(client, attacker, false);
	
	// 플레이어 수 계산.
	CheckPlayers();
}

/**
 * 감염 모듈; 라운드 프리즈 엔드 이벤트가 발동되었을 때
 */
void Infect_OnRoundFreezeEnd()
{
	// 인간 플레이어를 셉니다.
	int humancount;
	CountValidClients(_, humancount, true, true);
	
	// 클라이언트 수가 2명 이상이라면 감염타이머 작동.
	if (humancount >= 2)
	{
		// 감염 타이머를 초기화
		if (tInfect != INVALID_HANDLE)
			tInfect = INVALID_HANDLE;
		
		// 게임 시작 시간 콘솔 명령어 값 획득 후 감염타이머 발동.
		float timeamount = GetConVarFloat(cv[DELAY]);
		tInfect = CreateTimer(timeamount, Timer_Infect_InfectMother, _, TIMER_FLAG_NO_MAPCHANGE);
	}
}

/**
 * 감염 모듈; 라운드가 끝났을 때
 */
void Infect_OnRoundEnd()
{
	// 감염 타이머 제거
	if (tInfect != INVALID_HANDLE)
		KillTimer(tInfect);
	
	// 타이머 초기화.
	tInfect = INVALID_HANDLE;
	
	// 엔진상태 설정
	SetZombieSpawned(false);
	SetInfectStop(false);
	
	// x = 클라이언트 인덱스
	for (new x = 1; x <= MaxClients; x++)
	{
		if (!IsClientInGame(x))
			continue;
		
		// 좀비 종류를 인간으로 돌려놓습니다.
		CureClient(x);
	}
}

void CheckHumans()
{
	if (GetTeamClientCount(CS_TEAM_CT) == 1)
		SetInfectStop(true);
}


/***************************************************************************************************************
 * 감염 모듈; 한명의 플레이어를 감염시키는 타이머.
 * 
 * @param timer			타이머 핸들.
 ***************************************************************************************************************/
public Action Timer_Infect_InfectMother(Handle timer)
{
	// 타이머 변수 초기화.
	tInfect = INVALID_HANDLE;
	
	new Handle:aEligibleClients = INVALID_HANDLE;
	new eligibleclients = CreateEligibleClientList(aEligibleClients, true, true, true);
	
	// 적합한 클라이언트가 없다면 정지.
	if (!eligibleclients)
	{
		// 핸들 닫기.
		CloseHandle(aEligibleClients);
		PrintToServer("[ZM: Infect] There's no eligible client.");
		return;
	}
	
	// 랜덤 배열 인덱스에서 저장된 클라이언트를 변수로.
	new client;
	
    // x = 배열 인덱스.
    // client = 클라이언트 인덱스.
	for (new x = 0; x < eligibleclients; x++)
	{
		// 플레이어 1명뿐이라면 정지.
		if (eligibleclients <= 1)
			break;
		
		client = GetArrayCell(aEligibleClients, x);
	}
	
	new humancount;
	CountValidClients(_, humancount, true, true);
	
	// 일단 CT팀으로 전부 이동.
	for (new x = 1; x <= MaxClients; x++)
	{
		// 게임에 없는 사람 패스
		if (!IsClientInGame(x))
			continue;
		
		// 살아있지 않은 사람 패스
		if (!IsPlayerAlive(x))
			continue;
		
		// 팀 이동
		CS_SwitchTeam(x, CS_TEAM_CT);
	}
	
	// 다중감염을 위해 지정된 인구비율 값 획득
	float ratio = GetConVarFloat(cv[PLAYERRATIO]);
	
	// 플레이어 비율을 계산하여 mothercount에 저장.
	int mothercount = RoundToNearest(float(humancount) / ratio);
	
	// 그게 안됐으면 1명으로만 설정.
	if (!mothercount)
		mothercount = 1;
	
	// x = 현재 숙주 좀비 수
	for (int x = 0; x < mothercount; x++)
	{
		// 적합한 클라이언트를 다시 센다.
		eligibleclients = GetArraySize(aEligibleClients);
		
		// 적합한 클라이언트가 없으면 정지.
		if (!eligibleclients)
			break;
		
		int randindex;
		randindex = GetRandomInt(0, eligibleclients - 1);
		client = GetArrayCell(aEligibleClients, randindex);
		
		// 좀비를 여러마리 감염시킨다.
		InfectClient(client, _, true);
		
		// 이걸 뭐라고 설명해야하지?
		RemoveFromArray(aEligibleClients, randindex);
	}
	
	PrintCenterTextAll("첫번째 감염자 발생! : %N", client);
	PrintToChatAll("첫번째 감염자 발생! : %N", client);
	
	/*
		지금 이 주석내용은 사용할 때 꺼내 쓰세요
		
				PrintToChatAll("첫번째 감염자: %N!", client);
				PrintCenterTextAll("첫번째 감염자: %N!", client);
				PrintHintTextToAll("첫번째 감염자: %N!", client);
				PrintKeyHintText(0, "첫번째 감염자: %N!", client);
	*/
	
	// 좀비가 스폰되었음을 알립니다.
	SetZombieSpawned(true);
	
	// 인간 플레이어가 1명인지 감지하기 위해 플레이어 계산.
	CheckPlayers();
	
	// 핸들 닫기.
	CloseHandle(aEligibleClients);
}
