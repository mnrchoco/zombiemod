/*==================================================================

	==================================
	 [ZombieMod] Player Score Control
	==================================
	
	파일: zm_score.sp
	언어: SourcePawn
    방식: 절차형
    종류: 모듈
	설명: 각 플레이어들의 점수를 관리합니다.
		  감염될 시 공격자에게 사살 n점, 피해자(감염된 자)
		  에게 사망 n점 추가의 부분만 관리합니다.
		  라운드 종료는 정상적으로 되기 때문에 그 부분의 점수는
		  조정하지 않습니다.
		  
		  이 플러그인은 부가적인 플러그인입니다.
		  서버에 이 플러그인이 없어도 좀비의 기본 기능에는
		  아무런 영향을 미치지 않습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Score Manager",
	author 		= ZM_PRODUCER,
	description = "Manages Score.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Score", ZM_LANG_PREFIX);
}


//============================================================
// ZM 포워드
//============================================================
/**
 * 클라이언트가 감염된 후
 *
 * @param client 		클라이언트 인덱스
 * @param attacker 		공격자 인덱스
 * @param mother 		숙주로 감염됐나요?
 */
public void OnClientInfectPost(int client, int attacker, bool mother)
{
	// 공격자가 유효하면 점수 부여
	if (IsClientValid(attacker))
	{
		// 공격자에게 점수 증가.
		new score = utilClientScore(attacker, true, false);
		utilClientScore(attacker, true, true, ++score);
		
		// 피해자에게 사망횟수 증가.
		new deaths = utilClientScore(client, false, false);
		utilClientScore(client, false, true, ++deaths);
	}
}
