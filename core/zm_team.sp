/*==================================================================

	==========================
	 [ZombieMod] Team Control
	==========================
	
	파일: zm_team.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 플레이어가 관전자 팀 외에 다른 팀으로 들어가고 변경됨을
		  알리는 메시지 출력을 막습니다.
		  그리고 라운드 종료 시 팀 인원 균형을 맞춥니다.

	개정: CS:GO를 위하여 팀 이름 조정기능을 추가했음. (2018/June/7)

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <cstrike>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Team Manager",
	author 		= ZM_PRODUCER,
	description = "Manages team.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 상수
//============================================================
// CS:GO에 사용할 팀 이름 변경 용도.
#define TEAM_NAME_ZOMBIE "감염자"
#define TEAM_NAME_HUMAN "인간"


//============================================================
// 변수
//============================================================
// CS:GO에 사용할 팀 이름 변경 용도.
ConVar cvTeamNameHUMAN = null;
ConVar cvTeamNameZOMBIE = null;



//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 게임 이벤트
	if (GetEngineVersion() != Engine_CSGO)
		HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);
	else if (GetEngineVersion() == Engine_CSGO)
		RegConsoleCmd("jointeam", Command_JoinTeam);
	HookEvent("round_end", Event_RoundEnd);

	// 서버 명령어 찾기.
	cvTeamNameHUMAN = FindConVar("mp_teamname_1");
	cvTeamNameZOMBIE = FindConVar("mp_teamname_2");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Team", ZM_LANG_PREFIX);
}

/**
 * 맵 시작 시
 */
public void OnMapStart()
{
	// 팀 이름 설정. (서버 명령어 조정)
	cvTeamNameHUMAN.SetString(TEAM_NAME_HUMAN);
	cvTeamNameZOMBIE.SetString(TEAM_NAME_ZOMBIE);
}


//============================================================
// 콜백
//============================================================
/**
 * CS:GO를 위한 player_team 대체용 ; 플레이어가 팀에 들어갔을 때
 *
 * @param client 	클라이언트 인덱스
 * @param args 		인자
 */
public Action Command_JoinTeam(int client, int args)
{
	InitializeToHuman(client, CS_TEAM_CT);

	return Plugin_Handled;
}

/**
 * 게임이벤트; 플레이어가 팀에 들어갔을 때(Pre) : CS:S Only
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_PlayerTeam(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int team = event.GetInt("team");
	InitializeToHuman(client, team);
	
	return Plugin_Handled;
}

/**
 * 게임이벤트; 라운드 종료
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	BalanceTeams();
}


//============================================================
// 일반
//============================================================
/**
 * 클라이언트가 팀에 들어갔을 때
 *
 * @param client		클라이언트 인덱스
 * @param team			팀 인덱스
 */
void InitializeToHuman(int client, int team)
{
	// 팀이 관전자 팀이 아닙니다
	// === 이 말은 관전자로 들어간게 아닐 경우 어떤 팀에 들어갔다는 출력을 하지 않는다는 뜻.
	if (team != CS_TEAM_SPECTATOR)
		return;
	
	// 치유 (인간으로 설정)
	CureClient(client);
}

// 라운드가 끝난 후에 양쪽에 팀을 맞춥니다.
void BalanceTeams()
{
	new maxplayers = GetMaxClients();
  
	// Move all to Terrorist team
	for (new x = 1; x <= maxplayers; x++)
	{	
		if (IsClientInGame(x))
		{
			if (GetClientTeam(x) > 1)
				CS_SwitchTeam(x, CS_TEAM_T);
		}
	}
  
	// Move half to CT team
	for (new x = 1; x <= maxplayers; x++)
	{
		if (IsClientInGame(x))
		{
			if (GetClientTeam(x) > 1)
			{
				CS_SwitchTeam(x, CS_TEAM_CT);
				x++;
			}
		}
	}
}
