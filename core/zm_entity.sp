/*==================================================================

	============================
	 [ZombieMod] Entity Remover
	============================
	
	파일: zm_entity.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 좀비모드에 불필요한 엔티티를 제거합니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 맵에서 지울 대상.
#define TO_REMOVE_ENTITY	"func_bomb_target|func_hostage_rescue|hostage_entity|c4"


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name		= "[ZombieMod] Entity Manager",
	author 		= ZM_PRODUCER,
	description = "Manages entities.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 게임 이벤트
	HookEvent("round_start", Event_RoundStart);
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Entities", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 게임 이벤트; 라운드 시작 시
 * 
 * @param event				본 이벤트 핸들
 * @param name				이벤트의 이름
 * @param dontBroadcast		설정할 경우 해당 이벤트가 발생함을 알리지 않는다.
 */
public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	// 엔티티 제거!
	RemoveEntity();
}


//============================================================
// 일반
//============================================================
/**
 * 맵에서 필요없는 엔티티들을 제거합니다.
 */
void RemoveEntity()
{
	// 클래스 네임 버퍼 선언.
	char classname[64];
	
	// 유효한 모든 엔티티 인덱스를 반복(for)하여 찾는다.
	int maxentities = GetMaxEntities();
	for (int e = 1; e <= maxentities; e++)
	{
		// 유효한 엔티티가 아니라면 정지(패스).
		if (!IsValidEntity(e))
			continue;
		
		// Edict Classname을 획득
		GetEdictClassname(e, classname, sizeof(classname));
		
		// 제거할 대상과 일치하면 제거
		if (StrContains(TO_REMOVE_ENTITY, classname) > -1)
			RemoveEdict(e);
	}
}