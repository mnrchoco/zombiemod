/*==================================================================

	============================
	 [ZombieMod] Damage Control
	============================
	
	파일: zm_damage.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: SDKHooks 익스텐션을 이용하여 데미지를 관리합니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
	
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 데미지 타입
#define DMG_CSS_FALL		(DMG_FALL)		// 클라이언트 추락 시
#define DMG_CSS_BLAST		(DMG_BLAST)		// 폭발에 의한 데미지
#define DMG_CSS_BURN		(DMG_DIRECT)	// 불에 의해 데미지 받음.
#define DMG_CSS_BULLET		(DMG_NEVERGIB)	// 칼에 의한 데미지
#define DMG_CSS_HEADSHOT	(1 << 30)		// 헤드샷!


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Damage Control",
	author 		= ZM_PRODUCER,
	description = "Manages Damage.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Damage Control", ZM_LANG_PREFIX);
}

/**
 * 클라이언트가 서버에 들어왔을 때
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
	SDKHook(client, SDKHook_TraceAttack, SDKH_OnTraceAttack);
}

/**
 * 클라이언트가 접속을 끊었을 때
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
	SDKUnhook(client, SDKHook_TraceAttack, SDKH_OnTraceAttack);
}


//============================================================
// 콜백
//============================================================
/**
 * 데미지 모듈; 데미지를 입었을 때
 *
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 * @param inflictor		무기 인덱스
 * @param damage		데미지 수치
 * @param damagetype	데미지 종류
 */
public Action SDKH_OnTakeDamage(int client, &attacker, &inflictor, &Float:damage, &damagetype)
{
	// 좀비가 스폰되지 않았다면 데미지 무효
	if (!IsZombieSpawned())
		return Plugin_Handled;
	
	// 무기의 클래스명 획득
	decl String:classname[64];
	GetEdictClassname(inflictor, classname, sizeof(classname));
	
	// 맵에서 주는 데미지라면 허용 (trigger)
	if (StrContains(classname, "trigger") > -1)
		return Plugin_Continue;
	
	// 데미지 종류가 각각 위에 선언된 데미지 중류와 같다면 데미지 허용
	if (damagetype & DMG_CSS_FALL)
		return Plugin_Continue;
	
	if (damagetype & DMG_CSS_BURN)
		return Plugin_Continue;
	
	if (damagetype & DMG_CSS_BLAST)
		return Plugin_Continue;
	
	// 끝
	return Plugin_Continue;
}

/**
 * 데미지 모듈; 공격을 추적합니다.
 * 
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 * @param inflictor		무기 인덱스
 * @param damage		데미지 수치
 * @param damagetype	데미지 종류
 * @param ammotype		무기 종류?
 * @param hitbox		맞는 부위?
 * @param hitgroup		이건 또 뭐야
 */
public Action SDKH_OnTraceAttack(int client, &attacker, &inflictor, &Float:damage, &damagetype, &ammotype, hitbox, hitgroup)
{
	// 공격자가 유효하지 않으면 정지.
	if (!IsClientValid(attacker))
		return Plugin_Continue;
	
	// 공격자가 자신을 공격하는거면 정지.
	if (attacker == client)
		return Plugin_Continue;
	
	// 인간이라면 데미지 허용
	if (IsClientHuman(client))
		return Plugin_Continue;
	
	// 끝
	return Plugin_Continue;
}
