/*==================================================================

	===============================
	 [ZombieMod] Knockback Control
	===============================
	
	파일: zm_knockback.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	무기에 의해 밀려나는 넉백을 관리합니다.
		  이 넉백은 데미지에 따른 넉백이므로, 히트그룹(hitgroup)
		  에 따른 넉백으로 변경하려면 이 플러그인 소스코드를
		  변경하여야 합니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:KBMOTHER,
	Handle:KBNORMAL
}
new cv[CVAR];

/*
ConVar

# On CS:GO, Need to hooking console vars to old school source codes of player knockback.
sv_clamp_unsafe_velocities 0
cs_enable_player_physics_box 1
*/
ConVar cvClampUnsafeVelocities = null;
ConVar cvEnablePlayerPhysicsBox = null;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Player Knockback Manager",
	author 		= ZM_PRODUCER,
	description = "Manages the player knockback",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[KBMOTHER] = CreateConVar("zm_knockback_mother", "3.0", "숙주 좀비의 넉백");
	cv[KBNORMAL] = CreateConVar("zm_knockback_normal", "3.5", "일반 좀비의 넉백");

	// 조정할 서버 명령어
	cvClampUnsafeVelocities = FindConVar("sv_clamp_unsafe_velocities");
	cvEnablePlayerPhysicsBox = FindConVar("cs_enable_player_physics_box");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Manager :: Player Knockback", ZM_LANG_PREFIX);
}

/**
 * 맵 시작 시
 */
public void OnMapStart()
{
	// 서버 명령어 조정
	cvClampUnsafeVelocities.SetInt(0);
	cvEnablePlayerPhysicsBox(1);
}

/**
 * 클라이언트 서버 접속 시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
}

/**
 * 클라이언트 서버 퇴장 시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_OnTakeDamage, SDKH_OnTakeDamage);
}


//============================================================
// 콜백
//============================================================
/**
 * SDKHook : OnTakeDamage
 */
public Action SDKH_OnTakeDamage(int client, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, 
        float damageForce[3], float damagePosition[3], int damagecustom)
{
	// 데미지가 생기면 넉백 처리.
	KnockbackDoIt(client, attacker, damage);
}


//============================================================
// 일반
//============================================================
/**
 * 넉백 모듈; player_hurt로 부터 각 수치를 받아서 넉백을 처리합니다.
 *
 * @param client			클라이언트 인덱스
 * @param attacker			공격자 인덱스
 * @param damage			데미지 수치
 */
void KnockbackDoIt(int client, int attacker, float damage)
{
	// 공격자가 유효하지 않으면 정지.
	if (!IsClientValid(attacker)) 
		return;
	
	// 피해자가 인간이면 정지.
	if (IsClientHuman(client)) 
		return;
	
	float final_kb;
	
	float clientloc[3];
	float attackerloc[3];
	
	// 클라이언트의 위치 구하기.
	GetClientAbsOrigin(client, clientloc);
	
	// 공격자가 보고있는 곳의 좌표를 구하기.
	GetClientEyePosition(attacker, attackerloc);
	
	float attackerang[3];
	GetClientEyeAngles(attacker, attackerang);
	
	// 넉백 end-vector 계산.
	TR_TraceRayFilter(attackerloc, attackerang, MASK_ALL, RayType_Infinite, TRFilter);
	TR_GetEndPosition(clientloc);
	
	// 각 플레이어 속성별로 넉백 설정.
	if (IsClientMotherZombie(client))
		final_kb = GetConVarFloat(cv[KBMOTHER]);
	
	else if (IsClientZombie(client))
		final_kb = GetConVarFloat(cv[KBNORMAL]);

	final_kb *= damage;
	SetVelocity(client, attackerloc, clientloc, final_kb);
}

/**
 * 넉백을 표현하기 위한 벨로서티 설정.
 *
 * @param client			클라이언트 인덱스
 * @param startpoint		시작 지점
 * @param endpoint			끝 지점
 * @param magnitude			범위?
 */
void SetVelocity(int client, const float startpoint[3], const float endpoint[3], float magnitude)
{
	// Create vector from the given starting and ending points.
    float vector[3];
    MakeVectorFromPoints(startpoint, endpoint, vector);
    
    // Normalize the vector (equal magnitude at varying distances).
    NormalizeVector(vector, vector);
    
    // Apply the magnitude by scaling the vector (multiplying each of its components).
    ScaleVector(vector, magnitude);
    
    // ADD the given vector to the client's current velocity.
    utilClientVelocity(client, vector);
}

/**
 * TR필터; 맞은 자가 플레이어인지 아닌지 추적하기 위함(?)
 *
 * @param entity			엔티티 인덱스
 * @param contentMask		???
 */
public bool TRFilter(int entity, int contentsMask)
{
	// 엔티티가 플레이어라면 추적 계속. (If entity is a player, continue tracing.)
	if (entity > 0 && entity < MAXPLAYERS)
		return false;

	// Allow hit.
	return true;
}
