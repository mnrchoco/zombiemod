/*==================================================================

	======================
	 [ZombieMod] Core API
	======================
	
	파일: zm_api_core.sp
	언어: SourcePawn
	방식: 절차형
	종류: API 처리기
	설명: core.inc 라이브러리 처리기
	개정: 숙주좀비의 구분을 변수 각각에서 하나로 통합.
			기존: bZombie = false & bMother = true
			현재: 모두 true
			(2018/06/09)

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <cstrike>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 감염무기의 이름
#define INFECTION_WEAPON_NAME "zombies_claws_of_death"


//============================================================
// 변수
//============================================================
// 포워드 핸들
enum FWDHANDLE
{
	Handle:INFECTPRE,
	Handle:INFECTPOST,
	Handle:CUREPRE,
	Handle:CUREPOST
}
new fwd[FWDHANDLE];

// 클라이언트의 속성
bool bZombie[MAXPLAYERS + 1];
bool bMother[MAXPLAYERS + 1];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Core API",
	author		= ZM_PRODUCER,
	description = "Process The core API",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 로드 시 네이티브와 포워드를 만듭니다.
 */
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	// 네이티브
	CreateNative("IsClientMotherZombie", 	API_IsClientMotherZombie);
	CreateNative("IsClientZombie", 			API_IsClientZombie);
	CreateNative("IsClientHuman", 			API_IsClientHuman);
	CreateNative("SetClientType", 			API_SetClientType);
	CreateNative("InfectClient", 			API_InfectClient);
	CreateNative("CureClient", 				API_CureClient);
	
	// 포워드
	fwd[INFECTPRE] = 	CreateGlobalForward("OnInfectClientPost", 		ET_Hook, 		Param_CellByRef, Param_CellByRef, Param_CellByRef);
	fwd[INFECTPOST] = 	CreateGlobalForward("OnInfectClientPost", 		ET_Ignore, 		Param_Cell, Param_Cell, Param_Cell);
	fwd[CUREPRE] = 		CreateGlobalForward("OnCureClientPre", 			ET_Hook, 		Param_CellByRef);
	fwd[CUREPOST] = 	CreateGlobalForward("OnCureClientPost", 		ET_Ignore, 		Param_Cell);
	
	// 끝
	return APLRes_Success;
}

/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버에 알림
	PrintToServer("%s Enable :: API Processor :: Core", ZM_LANG_PREFIX);
}


//============================================================
// 네이티브 처리
//============================================================
/**
 * API; 숙주좀비인가요?
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_IsClientMotherZombie(Handle plugin, int numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 숙주좀비인가요?
	return P_IsClientMotherZombie(client);
}

/**
 * API; 좀비인가요?
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_IsClientZombie(Handle plugin, int numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 좀비인가요?
	return P_IsClientZombie(client);
}

/**
 * API; 인간인가요?
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_IsClientHuman(Handle plugin, int numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 인간인가요?
	return P_IsClientHuman(client);
}

/**
 * API; 클라이언트 속성 설정
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_SetClientType(Handle plugin, int numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 매개변수 2
	int type = GetNativeCell(2);

	// 처리
	P_SetClientType(client, type);
}

/**
 * API; 인간 감염
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_InfectClient(Handle plugin, numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 매개변수 2
	int attacker = GetNativeCell(2);

	// 매개변수 3
	bool mother = view_as<bool>(GetNativeCell(3));

	// 감염
	P_InfectClient(client, attacker, mother);
}

/**
 * API; 좀비 치료
 * 
 * @param plugin 		플러그인 핸들
 * @param numParams 	매개변수 개수
 */
public API_CureClient(Handle plugin, int numParams)
{
	// 매개변수 1
	int client = GetNativeCell(1);

	// 치유
	P_CureClient(client);
}


//============================================================
// 포워드 처리
//============================================================
/**
 * 포워드; 클라이언트를 감염하기 '전'
 *
 * @param client 		클라이언트 인덱스
 * @param attacker 		공격자 인덱스
 * @param mother 		숙주좀비로 감염됐나요?
 */
Action FWD_OnInfectClientPre(int &client, int &attacker, bool &mother)
{
	// 호출 시작
	Call_StartForward(fwd[INFECTPRE]);
	
	// 파라메터 (매개변수) 투입
	Call_PushCellRef(client);
	Call_PushCellRef(attacker);
	Call_PushCellRef(mother);
	
	// Get what they returned.
	new Action:result;
	Call_Finish(result);
	return result;
}

/** 
 * 포워드; 클라이언트를 감염한 '후'
 *
 * @param client 		클라이언트 인덱스
 * @param attacker 		공격자 인덱스
 * @param mother 		숙주좀비로 감염됐나요?
 */
void FWD_OnInfectClientPost(int client, int attacker, bool mother)
{
	// 호출 시작
	Call_StartForward(fwd[INFECTPOST]);
	
	// 파라메터 (매개변수) 투입
	Call_PushCell(client);
	Call_PushCell(attacker);
	Call_PushCell(mother);
	
	// 끝
	Call_Finish();
}

/** 
 * 포워드; 클라이언트를 치료하기 '전'
 *
 * @param client 		클라이언트 인덱스
 */
Action FWD_OnCureClientPre(int &client)
{
	// 호출 시작
	Call_StartForward(fwd[CUREPRE]);
	
	// 파라메터 (매개변수) 투입
	Call_PushCellRef(client);
	
	// Get what they returned.
	new Action:result;
	Call_Finish(result);
	return result;
}

/** 
 * 포워드; 클라이언트를 치료한 '후'
 *
 * @param client 		클라이언트 인덱스
 */
void FWD_OnCureClientPost(int client)
{
	// 호출 시작
	Call_StartForward(fwd[CUREPOST]);
	
	// 파라메터 (매개변수) 투입
	Call_PushCell(client);
	
	// 끝
	Call_Finish();
}


//============================================================
// 일반
//============================================================
// 프로세스; 클라이언트가 숙주 좀비인가요?
bool P_IsClientMotherZombie(int client)
{
	if (!IsClientValid(client))
		return false;
	
	return bMother[client];
}

// 프로세스; 클라이언트가 좀비인가요?
bool P_IsClientZombie(int client)
{
	if (!IsClientValid(client))
		return false;
	
	return bZombie[client];
}

// 프로세스; 클라이언트가 인간인가요?
bool P_IsClientHuman(int client)
{
	if (!IsClientValid(client))
		return false;
	
	return !bZombie[client];
}

// 프로세스; 클라이언트 속성 설정.
void P_SetClientType(int client, int type)
{
	if (type == ID_HUMAN)
	{
		bMother[client] = false;
		bZombie[client] = false;
	}
	
	else if (type == ID_ZOMBIE)
	{
		bZombie[client] = true;	
		bMother[client] = false;
	}
	
	else if (type == ID_MOTHERZOMBIE)
	{
		bMother[client] = true;	
		bZombie[client] = true;
	}
}

/**
 * 프로세스; 클라이언트 감염
 * 
 * @param client 		클라이언트 인덱스
 * @param attacker 		공격자 인덱스
 * @param mother 		설정시 숙주로 감염시킵니다
 */
void P_InfectClient(int client, int attacker = -1, bool mother = false)
{
	// 포워드 호출 (PRE)
	Action result = FWD_OnInfectClientPre(client, attacker, mother);
	
	// Plugin_Continue가 아니라면 정지
	if (result == Plugin_Handled)
		return;
	
	// 테러리스트로 전환
	CS_SwitchTeam(client, CS_TEAM_T);
	
	// 무기를 전부 뺐고, 칼 지급
	Weapon_RemoveAllClientWeapons(client);
	GivePlayerItem(client, "weapon_knife");
	GivePlayerItem(client, "item_nvgs");
	
	// 숙주로 표시
	if (mother)
		P_SetClientType(client, ID_MOTHERZOMBIE);
	
	// 아니면 좀비로 표시
	else
		P_SetClientType(client, ID_ZOMBIE);
	
	// 공격자가 유효하면
	if (IsClientValid(attacker))
	{
		// 이벤트를 허위로 발동
		new Handle:event = CreateEvent("player_death");
		if (event != INVALID_HANDLE)
		{
			SetEventInt(event, "userid", GetClientUserId(client));
			SetEventInt(event, "attacker", GetClientUserId(attacker));
			SetEventString(event, "weapon", INFECTION_WEAPON_NAME);
			FireEvent(event, false);
		}
	}
	
	// 포워드 호출 (POST)
	FWD_OnInfectClientPost(client, attacker, mother);
}

/**
 * 프로세스; 클라이언트를 인간으로 치료
 * 
 * @param client 		클라이언트 인덱스
 */
void P_CureClient(int client)
{
	// 포워드 호출
	Action result = FWD_OnCureClientPre(client);
	
	// Plugin_Continue가 아니라면 정지
	if (result == Plugin_Handled)
		return;
	
	// 인간으로 설정
	P_SetClientType(client, ID_HUMAN);
	
	// 포워드 호출
	FWD_OnCureClientPost(client);
}
