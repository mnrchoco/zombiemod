# Zombie Mod Engine
- for CS:S and CS:GO


### Information | 안내
- Written in SourceMod (SourceMod 로 작성되었습니다)
- CS:S fully support (CS:S에서는 정상 작동합니다)
- CS:GO 80% support (CS:GO에서는 80%만 작동합니다)
- The comments of source codes were only written in Korean (소스코드의 주석은 한국어로만 작성되었습니다)


### Bug list | 버그 목록
1. (CS:GO) Knockback bug to use CS:S code in raw. - 코드 추가로 실험중
2. (CS:GO) -Classic ammo refill style- bug (clip reseting when reloading clips) - 1.6 방식의 충전방식이 CS:GO에서 장전중에 초기화됨.
3. (CS:GO) Setting files are not generated(zp_option_money, zp_option_weather). - 설정파일이 생성되지 않음.



## How to use it | 사용 방법
1. Compile every files(SourceMod Version: 1.8.6047 or higher). - 모든 파일을 컴파일하세요.
2. Put them in 'Plugin' folder. - 'Plugin'폴더에 넣으세요.
3. Run your server. - 서버 실행


## TODO
- Human/Zombie Class Module?


## BUG REPORT | 버그 신고
- Here. To issue it at Issue tab(page).
- 여기에서 하세요. Issue탭(페이지)에 등록하시면 됩니다.
