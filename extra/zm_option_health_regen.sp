/*==================================================================

	===============================
	 [ZombieMod] Health Regenrator
	===============================
	
	파일: zm_option_health_regen.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 좀비 또는 인간을 위한 HP재생성 기능입니다.
			클래스가 추가되지 않은 상태에서 HP리젠 기능을 넣다보니
			특정 값 이하의 체력이 되면
			그 값까지만 체력리젠이 되도록 설계했습니다.
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
enum CVAR
{
	Handle:REGENHUMAN,
	Handle:REGENZOMBIE,
	Handle:REGENINTERVAL,
	Handle:REGENHPAMOUNT,
	Handle:REGENSTARTHP
}
new cv[CVAR];

Handle tRegen[MAXPLAYERS + 1];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Health Regenarator",
	author 		= ZM_PRODUCER,
	description = "Health Regenerator for ZombieMod.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[REGENHUMAN] = CreateConVar("zm_regen_human", "0", "인간 체력의 리젠기능을 활성화합니다.");
	cv[REGENZOMBIE] = CreateConVar("zm_regen_zombie", "1", "좀비 체력의 리젠기능을 활성화합니다.");
	cv[REGENINTERVAL] = CreateConVar("zm_regen_interval", "0.5", "체력이 리젠되는 시간간격.");
	cv[REGENHPAMOUNT] = CreateConVar("zm_regen_hp_amount", "3", "리젠될 체력");
	cv[REGENSTARTHP] = CreateConVar("zm_regen_start_hp", "1000", "리젠이 시작될 체력");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Option :: HP Regenerator", ZM_LANG_PREFIX);
}

/**
 * 클라이언트가 서버 접속시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	tRegen[client] = INVALID_HANDLE;
}

/**
 * 클라이언트가 서버 퇴장시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientDisconnect(int client)
{
	tRegen[client] = INVALID_HANDLE;
}


//============================================================
// 콜백
//============================================================
/**
 * 타이머: 체력 리젠
 *
 * @param timer 		타이머 핸들
 * @param client 		클라이언트 인덱스
 */
public Action Timer_RegenToClient(Handle timer, any client)
{
	// 클라이언트가 연결되지 않았거나 게임에 없거나 살아있지 않으면 정지.
	if (!IsClientValid(client))
		return Plugin_Continue;

	if (!IsClientInGame(client))
		return Plugin_Continue;

	if (!IsPlayerAlive(client))
		return Plugin_Stop;

	// 클라이언트가 좀비가 아닐 경우 정지.
	if (!IsClientZombie(client))
	{
		tRegen[client] = INVALID_HANDLE;
		return Plugin_Stop;
	}
	
	// 현재 체력과 최대 체력을 획득합니다.
	int curhp = GetClientHealth(client);
	int maxhp = GetConVarInt(cv[REGENSTARTHP]);
	
	// 체력이 얼마씩 증가될 것인지 cvar로 부터 얻어옵니다.
	int amount = GetConVarInt(cv[REGENHPAMOUNT]);
	
	// 현재 체력은 amount 수치까지 계속해서 더해줍니다.
	curhp += amount;
	
	// 현재 체력이 최대 체력보다도 적다면 재생성
	if (curhp < maxhp)
		SetEntityHealth(client, curhp);
	
	return Plugin_Continue;
}


//============================================================
// ZM 콜백
//============================================================
/**
 * 클라이언트가 감염된 후
 *
 * @param client 		클라이언트 인덱스
 * @param attacker 		공격자 인덱스
 * @param mother 		숙주로 감염됐나요?
 */
public OnInfectClientPost(int client, int attacker, bool mother)
{
	if (!IsClientValid(client))
		return;

	if (!IsClientInGame(client))
		return;

	if (!IsPlayerAlive(client))
		return;

	if (!IsClientZombie(client))
		return;

	tRegen[client] = INVALID_HANDLE;
	
	float interval = GetConVarFloat(cv[REGENINTERVAL]);
	tRegen[client] = CreateTimer(interval, Timer_RegenToClient, client, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

/**
 * 클라이언트가 인간이 된 후 
 *
 * @param client 		클라이언트 인덱스
 */
public OnCureClientPost(int client)
{
	if (!IsClientValid(client))
		return;

	if (!IsClientInGame(client))
		return;

	tRegen[client] = INVALID_HANDLE;
}
