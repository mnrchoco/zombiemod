/*==================================================================

	=====================================================
	 [ZombieMod] Zombie RightMouse (+Attack2) Blocker -*-
	=====================================================
	
	파일: zm_option_attackblock.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 좀비가 +attack2 로 공격하는 것에 대한 제어기
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Zombie +attack2 Blocker",
	author 		= ZM_PRODUCER,
	description = "Blocks the +attack2 skill by zombies.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 서버에 알림 
	PrintToServer("%s Enable :: Option :: Zombie +attack2 Blocker", ZM_LANG_PREFIX);
}

/**
 * 클라이언트가 서버 접속시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_PreThink, SDKH_PreThink);
}

/**
 * 클라이언트가 서버 퇴장시
 *
 * @param client 		클라이언트 인덱스
 */
public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_PreThink, SDKH_PreThink);
}


//============================================================
// 콜백
//============================================================
/**
 * SDKHook: PreThink
 *
 * @param client 		클라이언트 인덱스
 */
public void SDKH_PreThink(int client)
{
	int iButtons = GetClientButtons(client);
	if (!(iButtons & IN_ATTACK2))
		return;
	
	if (IsClientZombie(client) && !IsStopInfect())
	{
		iButtons &= ~IN_ATTACK2;
		SetEntProp(client, Prop_Data, "m_nButtons", iButtons);
	}
}
