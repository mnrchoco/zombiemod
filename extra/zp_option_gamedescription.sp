/*==================================================================

	--------------------------------------------
	-*- [ZombieMod] Game Description Changer -*-
	--------------------------------------------
	
	파일: zm_gamedescription.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 서버의 게임이름을 원하는 대로 변경합니다.
			SDKHook의 OnGetGameDescription은
			Episode 2 엔진에서 작동하지 않습니다
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:DESCRIPTION
};
new cv[CVAR];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Game Description Changer",
	author 		= ZM_PRODUCER,
	description = "Replaces the Game description of server.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인이 시작될 떄
 */
public void OnPluginStart()
{
	// Game Description Override가 필요로 하는 콘솔명령어를 등록.
	cv[DESCRIPTION] = CreateConVar("zm_gamedescription", "ZombieMod", "ZombieMod가 작동하면 게임이름을 설정합니다");
	
	// 설정 파일 자동 생성.
	AutoExecConfig(true, "zombiemod/game_description");
	
	// 서버에 알림.
	PrintToServer("%s Enable :: Option :: Game Description Changer", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 게임이름을 획득하였을 때
 * 
 * @param gameDesc	게임이름의 문자열
 */
public Action OnGetGameDescription(String:gameDesc[64])
{
	// 게임이름을 콘솔명령어로부터 획득한다.
	char cv_string[64];
	GetConVarString(cv[DESCRIPTION], cv_string, sizeof(cv_string));
	
	// 형식 설정 후
	Format(gameDesc, sizeof(gameDesc), "%s", cv_string);
	
	// 그 것으로 변경하기.
	return Plugin_Changed;
}
