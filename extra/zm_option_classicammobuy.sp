/*==================================================================

	==========================================
	 [ZombieMod] CS:GO Flashlight Alternative
	==========================================
	
	파일: zm_csgo_flashlight.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: Flashlight가 되지 않는 CS:GO 에서
		  되게끔 해줍니다.

	제작: Vlen
	웹: http://github.com/vlennus
		  
==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <zombiemod>


//============================================================
// 상수
//============================================================
#define ADDAMMO_MOUNT 10
#define PAY 10


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Option : Classic Style Ammo Buy",
	author 		= ZM_PRODUCER,
	description = "CS 1.6 style ammo buy system for ZombieMod",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}
	

//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작시
 */
public void OnPluginStart()
{
	// 클라이언트 명령어 : 쉼표 키 + 마침표 키
	AddCommandListener(Command_BuyAmmo, "buyammo1");
	AddCommandListener(Command_BuyAmmo, "buyammo2");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Option :: Classic style ammo buy", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
/**
 * 클라이언트 명령어 : 탄창 구입.
 *
 * @param client 		클라이언트 인덱스.
 * @param command 		명령어 이름.
 * @param argc 			파라미터.
 */
public Action Command_BuyAmmo(int client, char[] command, int argc)
{
	// 인간이 아니면 정지
	if (!IsClientValid(client))
		return Plugin_Continue;

	if (!IsClientInGame(client))
		return Plugin_Continue;

	if (!IsPlayerAlive(client))
		return Plugin_Continue;

	if (!IsClientHuman(client))
		return Plugin_Continue;

	// 들고있는 무기의 종류에 따라 탄창을 주고
	any ammo;
	if (GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY))
		ammo = getCurrentWeaponReservedAmmo(client, CS_SLOT_PRIMARY);
	else if (GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY))
		ammo = getCurrentWeaponReservedAmmo(client, CS_SLOT_SECONDARY);

	setCurrentWeaponReservedAmmo(client, ammo + ADDAMMO_MOUNT);

	// 돈 청구.
	utilSetClientMoney(client, utilGetClientMoney(client) - PAY);

	// 알림.
	PrintToChat(client, "\x04%s\x01 10알의 탄이 들어있는 탄창을 구입하였습니다.\n / You bought the 10 reserved ammos of your current weapon.", ZM_LANG_PREFIX);

	return Plugin_Continue;
}


//============================================================
// 일반
//============================================================
/**
 * 현재 클라이언트가 들고있는 무기의 인덱스를 구하기.
 *
 * @param client 			클라이언트 인덱스.
 */
int getCurrentWeaponReservedAmmo(int client, int weapontype)
{
	int weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
	if (!IsValidEntity(weapon))
		return -1;

	if (weapontype == CS_SLOT_PRIMARY)
		return GetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount");
	else if (weapontype == CS_SLOT_SECONDARY)
		return GetEntProp(weapon, Prop_Send, "m_iSecondaryReserveAmmoCount");

	else return -1;
}

/**
 * 현재 클라이언트가 들고있는 무기의 탄창 수 설정.
 *
 * @param client 			클라이언트 인덱스.
 * @param weapon 			무기 인덱스.
 * @param mount 			설정할 값.
 */
void setCurrentWeaponReservedAmmo(int client, int mount)
{
	any weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
	if (!IsValidEntity(weapon))
		return;
		
	if (GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY))
	{
		SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", mount);
		SetEntProp(weapon, Prop_Send, "m_iClip2", mount);
	}
	else if (GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY))
	{
		SetEntProp(weapon, Prop_Send, "m_iSecondaryReserveAmmoCount", mount);
		SetEntProp(weapon, Prop_Send, "m_iClip2", mount);
	}	
}
