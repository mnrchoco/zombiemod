/*==================================================================

	====================
	 [ZombieMod] ZSPAWN
	====================
	
	파일: zm_zspawn.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 게임 도중에 들어왔을때 사용 가능한 리스폰 기능입니다.
		  이 플러그인의 경우에는 클라이언트가 서버에 나갔다 다시
		  들어오면 해당 기능 사용 기록이 사라집니다.
		  Zombie: Reloaded와 다른 동작 방식입니다.

	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 리스폰 했는지 여부
bool bZSpawn[MAXPLAYERS + 1];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] ZSpawn",
	author 		= ZM_PRODUCER,
	description = "ZSpawn",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 콘솔 명령어
	RegConsoleCmd("say", Command_Say);
	RegConsoleCmd("team_say", Command_Say);
	
	// 서버에 알림
	PrintToServer("%s Enable :: Module :: ZSpawn", ZM_LANG_PREFIX);
}

/**
 * 클라이언트가 서버에 들어왔을 때
 */
public void OnClientPutInServer(int client)
{
	// 지스폰 기록 초기화.
	bZSpawn[client] = false;
}

/**
 * 클라이언트가 서버에서 나갔을 때
 */
public void OnClientDisconnect(int client)
{
	// 지스폰 기록을 초기화.
	bZSpawn[client] = false;
}


//============================================================
// 콜백
//============================================================
/**
 * 콘솔명령어 say를 훅하기 위한 사용자 함수
 *
 * @param client 		클라이언트 인덱스
 * @param args 			인자
 */
public Action Command_Say(int client, int args)
{
	char says[192];
	GetCmdArgString(says, sizeof(says));
	ReplaceString(says, sizeof(says), "\"", "");
	
	// 해당 명령어들을 채팅창에 하나라도 입력했을 경우
	if (StrEqual(says, "!리스폰") || StrEqual(says, "!zspawn", false) || StrEqual(says, "/리스폰") || StrEqual(says, "/zspawn", false))
	{
		// 동작
		ZSpawn_SpawnClient(client);
	}
}


//============================================================
// 일반
//============================================================
/**
 * 리스폰 모듈; 클라이언트를 리스폰시킵니다.
 * 
 * @param client		클라이언트 인덱스
 */
void ZSpawn_SpawnClient(int client)
{
	// 플레이어가 게임에 없으면 정지.
	if (!IsClientInGame(client))
		return;
	
	// 플레이어가 살아있으면 정지.
	if (IsPlayerAlive(client))
		return;
	
	// 리스폰 불이 true인 경우 정지.
	if (bZSpawn[client])
	{
		PrintToChat(client, "%s 이미 리스폰 명령어를 사용하였습니다.", ZM_LANG_PREFIX_CHAT);
		return;
	}

	// 좀비가 스폰되지 않았다면 인간은 무한 리스폰.
	if (!IsZombieSpawned())
	{
		CS_SwitchTeam(client, CS_TEAM_CT);
		CS_RespawnPlayer(client);
	}
	
	// 허나 좀비가 스폰 됐다면 
	else if (IsZombieSpawned()) 
	{
		// 그리고 지스폰 기록이 없다면 
		if (!bZSpawn[client])
		{
			// 플레이어를 리스폰 시키고 좀비로 감염시킨다.
			CS_RespawnPlayer(client);
			InfectClient(client);
			
			// 지스폰을 했다고 기록합니다.
			bZSpawn[client] = true;
		}
	}
}
