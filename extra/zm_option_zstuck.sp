/*==================================================================

	====================
	 [ZombieMod] ZSTUCK
	====================
	
	파일명: zm_zstuck.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 게임 도중에 플레이어들끼리 끼었을 때 사용하는 기능입니다.
		  Zombie: Reloaded 게시판에 등록된 추가플러그인을
		  따왔습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <zombiemod>


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:ENABLE,
	Handle:INTERVAL,
	Handle:NOBLOCK,
	Handle:SLAP
}
new cv[CVAR];

bool bStucking[MAXPLAYERS + 1];


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] ZStuck",
	author 		= ZM_PRODUCER,
	description = "ZStuck",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[ENABLE] = CreateConVar("zm_zstuck", "1", "ZSTUCK 활성화.");
	cv[INTERVAL] = CreateConVar("zm_zstuck_interval", "3", "텔레포트 사용 후 제한시간");
	cv[NOBLOCK] = CreateConVar("zm_zstuck_noblock", "1", "본 기능 사용 시 노블럭을 활성화 합니다");
	cv[SLAP] = CreateConVar("zm_zstuck_slap", "1", "데미지 주기(SLAP)를 사용합니다.");
	
	// say 콘솔 명령어 훅.
	RegConsoleCmd("say", Command_Say);
	RegConsoleCmd("team_say", Command_Say);
	RegConsoleCmd("zstuck", Command_ZStuck);
	
	// 서버에 알림.
	PrintToServer("%s ZStuck is enable", ZM_LANG_PREFIX);
}


//============================================================
// 콜백
//============================================================
public Action Command_Say(int client, numParams)
{
	char says[192];
	GetCmdArgString(says, sizeof(says));
	ReplaceString(says, sizeof(says), "\"", "");
	
	if (StrEqual(says, "!꼈어") || StrEqual(says, "/꼈어"))
	{
		if (GetConVarBool(cv[ENABLE]))
			ZStuck_Do(client);
	}
}

public Action Command_ZStuck(int client, numParams)
{
	if (GetConVarBool(cv[ENABLE]))
		ZStuck_Do(client);
}

public Action Timer_DisableNoblock(Handle timer, any client)
{
	utilSetNoCollide(client, false);
	return Plugin_Stop;
}

public Action Timer_DisableStuck(Handle timer, any client)
{
	bStucking[client] = false;
	return Plugin_Stop;
}


//============================================================
// 일반
//============================================================
// 텔레포트 실시
void ZStuck_Do(int client)
{
	if (bStucking[client])
		return;
	
	bool h_bNoblock = GetConVarBool(cv[NOBLOCK]);
	bool h_bSlap = GetConVarBool(cv[SLAP]);
	if (h_bNoblock)
	{
		utilSetNoCollide(client, true);
		CreateTimer(0.3, Timer_DisableNoblock, client, TIMER_FLAG_NO_MAPCHANGE);
	}
	
	if (h_bSlap)
		SlapPlayer(client, 0, false);
	
	bStucking[client] = true;
	float fInterval = GetConVarFloat(cv[INTERVAL]);
	CreateTimer(fInterval, Timer_DisableStuck, any:client, TIMER_FLAG_NO_MAPCHANGE);
}

stock IsPlayerStuck(int client) 
{
    decl Float:vecMin[3], Float:vecMax[3], Float:vecOrigin[3];
    
    GetClientMins(client, vecMin);
    GetClientMaxs(client, vecMax);
    
    GetClientAbsOrigin(client, vecOrigin);
    
    TR_TraceHullFilter(vecOrigin, vecOrigin, vecMin, vecMax, MASK_PLAYERSOLID, TraceRayDontHitPlayerAndWorld);
    return TR_GetEntityIndex();
}

public bool TraceRayDontHitPlayerAndWorld(entityhit, mask) 
{
    return (entityhit < 1 || entityhit > MaxClients);
}
