/*==================================================================

	=================================
	 [ZombieMod] Player Money Add-On
	=================================
	
	파일: zm_option_money.sp
	언어: SourcePawn
	방식: 절차형
	종류: 모듈
	설명: 게임 내 플레이어 돈을 여러 방식으로 지급.
	
			인간이 좀비를 공격하면 부위별로 돈이 지급됩니다.
			이는 지정-최소값부터 각 부위별 지정값까지 무작위로
			지급합니다.
			
			예시: 좀비의 머리는 한 번당 1원~320원 무작위 지급.
	
	제작: Vingbbit
	웹: http://github.com/vlennus

==================================================================*/

#pragma semicolon 1


//============================================================
// 라이브러리
//============================================================
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <zombiemod>


//============================================================
// 상수
//============================================================
// 돈을 지급할 최소의 값 (부위별로 지급될 돈의 최소값)
#define MONEY_MIN_VALUE 1

// CS:GO 전용
#define MAXMONEY_OVERRIDE 30000

// 라운드 시작할 때마다 지급할 돈
#define MONEY_PER_START 5000


//============================================================
// 변수
//============================================================
// 서버 명령어
enum CVAR
{
	Handle:HITHEAD,
	Handle:HITUPPERCHEST,
	Handle:HITLOWERCHEST,
	Handle:HITLEFTARM,
	Handle:HITRIGHTARM,
	Handle:HITLEFTLEG,
	Handle:HITRIGHTLEG,
	Handle:ON_KILL
}
new cv[CVAR];

// 서버 명령어 찾기 : 최대보유 돈
ConVar cvMaxMoney = null;


//============================================================
// 플러그인 정보
//============================================================
public Plugin myinfo = 
{
	name 		= "[ZombieMod] Money Rewards",
	author 		= ZM_PRODUCER,
	description = "Manages Player Money In-game.",
	version 	= ZM_VERSION,
	url 		= ZM_URL
}


//============================================================
// 소스모드 포워드
//============================================================
/**
 * 플러그인 시작 시
 */
public void OnPluginStart()
{
	// 서버 명령어
	cv[HITHEAD] = CreateConVar("zm_money_hit_head", "200", "Money amount of head from per hit\n머리를 때릴 때 지급할 돈");
	cv[HITUPPERCHEST] = CreateConVar("zm_money_hit_upperchest", "10", "Money amount of upper chest from per hit\n윗쪽 뼈를 때릴 때 지급할 돈");
	cv[HITLOWERCHEST] = CreateConVar("zm_money_hit_lowerchest", "10", "Money amount of lower chest from per hit\n아래쪽 뼈를 때릴 때 지급할 돈");
	cv[HITLEFTARM] = CreateConVar("zm_money_hit_leftarm", "7", "Money amount of left arm from per hit\n왼쪽 팔을 때릴 때 지급할 돈");
	cv[HITRIGHTARM] = CreateConVar("zm_monet_hit_rightarm", "7", "Money amount of right arm from per hit\n오른쪽 팔을 때릴 때 지급할 돈");
	cv[HITLEFTLEG] = CreateConVar("zm_money_hit_leftleg", "15", "Money amount of left leg from per hit\n왼쪽 다리를 때릴 때 지급할 돈");
	cv[HITRIGHTLEG] = CreateConVar("zm_money_hit_rightleg", "15", "Money amount of right leg from per hit\n오른쪽 다리를 때릴 때 지급할 돈");
	cv[ON_KILL] = CreateConVar("zm_money_kill_zm", "500", "Money amount of killed zombie\n좀비를 죽였을 때 지급할 돈");
	
	cvMaxMoney = FindConVar("mp_maxmoney");
	
	HookEvent("round_start", Event_RoundStart);
	HookEvent("player_death", Event_PlayerDeath);
	
	// 설정 파일 자동 생성.
	AutoExecConfig(true, "zombiemod/option_money");
	
	// 서버에 알림
	PrintToServer("%s Enable :: Option :: Money Reward", ZM_LANG_PREFIX);
}

/**
 * 맵 시작 시
 */
public void OnMapStart()
{
	if (cvMaxMoney != null)
		cvMaxMoney.SetInt(MAXMONEY_OVERRIDE);
}

/**
 * 
 */
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_TraceAttack, SDKH_TraceAttack);
}

/**
 * 
 */
public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_TraceAttack, SDKH_TraceAttack);
}


//============================================================
// 콜백
//============================================================
/**
 * SDKHook : TraceAttack
 */
public Action SDKH_TraceAttack(int client, int &attacker, int &inflictor, float &damage, int &damagetype, int &ammotype, int hitbox, int hitgroup)
{
	if (!IsClientInGame(client) || !IsClientInGame(attacker))
		return Plugin_Continue;

	if (!IsPlayerAlive(client) || !IsPlayerAlive(attacker))
		return Plugin_Continue;

	moneyOnAttacking(client, attacker, hitgroup);

	return Plugin_Continue;
}

/**
 * 게임 이벤트; round_start
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	for (int x = 1; x <= MaxClients; x++)
	{
		if (!IsClientInGame(x)) return;
		if (!IsPlayerAlive(x)) return;
		if (utilGetClientMoney(x) > 5000) return;

		utilSetClientMoney(x, 5000);
	}
}

/**
 * 게임 이벤트; player_death
 *
 * @param event 			이벤트 핸들
 * @param name 				이벤트 이름
 * @param dontBroadcast 	설정시 화면에 표시하지 않습니다
 */
public Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int id = GetClientOfUserId(GetEventInt(event, "userid"));
	int at = GetClientOfUserId(GetEventInt(event, "attacker"));

	if (!IsClientConnected(id || !IsClientConnected(at)))
		return Plugin_Continue;

	if (!IsClientInGame(id) || !IsClientInGame(at))
		return Plugin_Continue;

	if (!IsPlayerAlive(id) || !IsPlayerAlive(at))
		return Plugin_Continue;
	
	PlayerMoney_OnClientDeath(id, at);

	return Plugin_Continue;
}


//============================================================
// 일반
//============================================================
/**
 * 돈; 클라이언트가 데미지를 입었을 때
 *
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 */
void moneyOnAttacking(int client, int attacker, int hitgroup)
{
	/*
	player_hurt index; Hitgroups / is work well on TraceAttack method? 
	1 = Head
	2 = Upper Chest
	3 = Lower Chest
	4 = Left arm
	5 = Right arm
	6 = Left leg
	7 = Right Leg
	*/

	// 공격자가 자기자신이라면 정지.
	if (client == attacker) 
		return;
	
	// 공격자의 현재 현금값을 획득.
	int attackercash = utilGetClientMoney(attacker);
	
	// 부위별 값 획득.
	int cvvhead = GetConVarInt(cv[HITHEAD]);
	int cvvuchest = GetConVarInt(cv[HITUPPERCHEST]);
	int cvvlchest = GetConVarInt(cv[HITLOWERCHEST]);
	int cvvleftarm = GetConVarInt(cv[HITLEFTARM]);
	int cvvrightarm = GetConVarInt(cv[HITRIGHTARM]);
	int cvvleftleg = GetConVarInt(cv[HITLEFTLEG]);
	int cvvrightleg = GetConVarInt(cv[HITRIGHTLEG]);
	
	// 내용 개선(?)을 위한 최종 지급값 변수
	int finalmoneyvalue;
	
	// 피해자가 좀비 또는 숙주라면
	if (IsClientZombie(client) || IsClientMotherZombie(client))
	{
		// 맞은 부위가 머리일 경우
		if (hitgroup == 1) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvhead));
		
		// 맞은 부위가 upper chest일 경우
		else if (hitgroup == 2) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvuchest));
		
		// 맞은 부위가 lower chest일 경우
		else if (hitgroup == 3) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvlchest));
		
		// 맞은 부위가 왼쪽 팔일 경우
		else if (hitgroup == 4) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvleftarm));
		
		// 맞은 부위가 오른쪽 팔일 경우
		else if (hitgroup == 5) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvrightarm));
		
		// 맞은 부위가 왼쪽팔일 경우
		else if (hitgroup == 6) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvleftleg));
		
		// 맞은 부위가 오른쪽 팔일 경우
		else if (hitgroup == 7) 
			finalmoneyvalue = (attackercash + GetRandomInt(MONEY_MIN_VALUE, cvvrightleg));
	}
	
	utilSetClientMoney(attacker, finalmoneyvalue);
}

/**
 * 돈; 클라이언트가 사망했을 때
 *
 * @param client		클라이언트 인덱스
 * @param attacker		공격자 인덱스
 */
void PlayerMoney_OnClientDeath(client, attacker)
{
	// 공격자가 자기자신이라면 정지.
	if (client == attacker) 
		return;
	
	// 사망지급값 획득.
	int killreward = GetConVarInt(cv[ON_KILL]);

	// 사망자가 좀비 또는 숙주라면 
	if (IsClientZombie(client) || IsClientMotherZombie(client))
	{
		int attackercash = utilGetClientMoney(attacker);
		
		// 공격자의 현금을 데미지 수치와 더해준다.
		utilSetClientMoney(attacker, (attackercash + GetRandomInt(MONEY_MIN_VALUE, killreward)));
	}
}
