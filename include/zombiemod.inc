/*==================================================================

	===========
	 ZombieMod
	===========
	
	파일: zombiemod.inc
	언어: SourcePawn
	방식: 절차형
	종류: 메인
	설명: 모든 헤더를 한대 모아놓습니다.
			스크립트 작성 시 #include <zombiemod>를 하면 됩니다

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

/**
 * @section 클라이언트 속성 (SetClientType의 2번째 파라메터 'type'에 사용)
 */
#define ID_HUMAN 		 0
#define ID_ZOMBIE		 1
#define ID_MOTHERZOMBIE  2
/**
 * @endsection
 */

// 라이브러리
// 상수
#include <zombiemod/information>

// API
#include <zombiemod/core>
#include <zombiemod/engine>
#include <zombiemod/base>
#include <zombiemod/util>
#include <zombiemod/weapon>
