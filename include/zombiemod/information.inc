/*==================================================================

	===========================================
	 [ZombieMod] Common constant (Information)
	===========================================
	
	파일: information.inc
	언어: SourcePawn
	방식: 절차형
	종류: 상수 모음
	설명: 제작자, 버젼, 제작자 홈페이지, 메시지 접두어 정의

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

// 제작자 정보
#define ZM_PRODUCER 			"Vingbbit"
#define ZM_VERSION 				"1.4"
#define ZM_URL 					"http://github.com/vlennus"

// 메시지 출력시 사용
#define ZM_LANG_PREFIX 			"[ZombieMod]"
#define ZM_LANG_PREFIX_CHAT 	"\x05[ZombieMod]\x03"
