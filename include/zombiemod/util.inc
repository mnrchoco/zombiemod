/*==================================================================

	=======================
	 [ZombieMod] Utilities
	=======================
	
	파일: util.inc
	언어: SourcePawn
	방식: 절차형
	종류: 라이브러리
	설명: 도구 집합

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

/**
 * 클라이언트의 벨로서티를 설정
 * 
 * @param client		클라이언트 인덱스
 * @param vecVelocity	벨로서티 값
 * @param apply			설정한 값을 적용합니다.
 * @param stack 		쌓아놓습니다? 뭔 뜻이야 이게?
 */
stock void utilClientVelocity(int client, float[3] vecVelocity, bool apply = true, bool stack = true)
{
	new iVelocity = FindSendPropInfo("CBasePlayer", "m_vecVelocity[0]");
	if (iVelocity == -1)
	{
		SetFailState("Could not find CBasePlayer::m_vecVelocity[0]");
		return;
	}

	if (!apply)
	{
		// x = 벡터 컴포넌트
		for (new x = 0; x < 3; x++)
			vecVelocity[x] = GetEntDataFloat(client, iVelocity + (x * 4));
		
		// 여기서 정지.
		return;
	}
	
	if (stack)
	{
		// 클라이언트의 벨로서티.
		new Float:vecClientVelocity[3];
		
		// x = 벡터 컴포넌트
		for (new x = 0; x < 3; x++)
			vecClientVelocity[x] = GetEntDataFloat(client, iVelocity + (x * 4));
		
		AddVectors(vecClientVelocity, vecVelocity, vecVelocity);
	}
	
	// 클라이언트에게 벨로서티 적용.
	TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, vecVelocity);
}

/**
 * 클라이언트의 LMV 수치를
 * 이것을 이용하여 속도를 조절할 경우
 * 점프 높이는 정상적이지 않습니다
 *
 * @param client	클라이언트 인덱스
 * @param value		LMV 값(300.0 = 기본, 600.0 = 더블)
 */
stock int utilSetClientLMV(int client, float value)
{
	// 클라이언트의 LMV 수치를 적용.
	SetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue", value / 300.0);
}


/**
 * 클라이언트의 FOV(시야)를 기본으로 지정합니다.
 *
 * @param client	클라이언트 인덱스
 * @param FOV		클라이언트의 기본 시야
 */
stock int utilSetClientFOV(int client, int FOV)
{
	SetEntProp(client, Prop_Send, "m_iFOV", FOV);
}


/**
 * 클라이언트의 알파값(투명도)를 설정합니다.
 *
 * @param client		클라이언트 인덱스
 * @param alpha			알파 값
 * @param weapons		모든 클라이언트의 무기에 알파값을 지정합니다.
 */
stock void utilSetClientAlpha(int client, int alpha)
{
	// 알파값을 조절하는 렌더링 모드
	SetEntityRenderMode(client, RENDER_TRANSALPHA);
	
	// 알파값 설정
	SetEntityRenderColor(client, _, _, _, alpha);
}


/**
 * 클라이언트의 사살점수 또는 죽은횟수를 획득하거나 설정합니다.
 * 
 * @param client	클라이언트 인덱스
 * @param score		true면 사살점수를, false면 죽은 횟수를 본다.
 * @param apply		true면 사살점수나 죽은횟수를 설정하고, false면 그들을 획득한다.
 * @param value		클라이언트의 사살점수나 죽은횟수의 값
 *
 * @return			클라이언트의 사살점수나 사망수, -1 if setting.
 */
stock void utilClientScore(int client, bool score = true, bool apply = true, int value = 0)
{
	if (!apply)
	{
		// score가 true면 클라이언트 사살점수를 반영.
		if (score)
			return GetEntProp(client, Prop_Data, "m_iFrags");
		
		// 클라이언트의 죽은 횟수를 반영.
		else
			return GetEntProp(client, Prop_Data, "m_iDeaths");
	}
	
	// score가 true면 클라이언트의 사살점수를 설정.
	if (score)
		SetEntProp(client, Prop_Data, "m_iFrags", value);
	
	// 클라이언트 사망 수를 설정.
	else
		SetEntProp(client, Prop_Data, "m_iDeaths", value);
	
	// 우리는 클라이언트의 점수 또는 사망수를 설정합니다.
	return -1;
}

/**
 * 충돌이 일어나지 않게합니다.
 * 
 * @param client		클라이언트 인덱스
 * @param nocollide		설정시 충돌방지
 */
stock void utilSetNoCollide(int client, bool nocollide = false)
{
	new iCollision = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	if (iCollision == -1)
	{
		SetFailState("Could not find CBaseEntity::m_CollisionGroup");
		return;
	}

	if (nocollide) 
		SetEntData(client, iCollision, 2, 1, true);
	
	else 
		SetEntData(client, iCollision, 5, 1, true);
}

/**
 * 클라이언트의 자금을 획득
 *
 * @param client 		클라이언트 인덱스
 */
stock utilGetClientMoney(int client)
{
	return GetEntProp(client, Prop_Send, "m_iAccount");
}

/**
 * 클라이언트의 자금을 설정
 *
 * @param client 		클라이언트 인덱스
 * @param value 		값
 */
stock void utilSetClientMoney(int client, int value)
{
	if (value < 1)
		value = 0;

	SetEntProp(client, Prop_Send, "m_iAccount", value);
}
