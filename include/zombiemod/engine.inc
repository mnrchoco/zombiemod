/*==================================================================

	========================
	 [ZombieMod] Engine API
	========================
	
	파일: engine.inc
	언어: SourcePawn
	방식: 절차형
	종류: 라이브러리
	설명: 첫번째 좀비가 스폰되었는지, 감염방지장치가 작동하는지
			등을 검사하고, 그것을 설정하는 함수가 있습니다.

	제작: Vingbbit
	웹: http://github.com/vlennus
		  
==================================================================*/

/**
 * 첫번째 좀비가 스폰됐나요?
 */
native bool IsZombieSpawned();

/**
 * 감염이 정지됐나요?
 */
native bool IsStopInfect();

/**
 * 좀비가 나타났음을 설정
 * 
 * @param set		설정하면 첫번째 좀비가 스폰된 것으로, 아닌 경우 해제
 */
native SetZombieSpawned(bool set = false);

/**
 * 감염 방지장치를 설정
 * 
 * @param set		설정할 경우 감염을 정지하고, 아닌 경우 해제
 */
native SetInfectStop(bool set = false);
